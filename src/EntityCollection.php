<?php

/**
 * This file is part of the YetORM library.
 *
 * Copyright (c) 2014 Jaroslav Hranička
 * Copyright (c) 2013, 2014 Petr Kessler (http://kesspess.1991.cz)
 *
 * @license  MIT
 * @link     https://bitbucket.org/hranicka/yetorm
 * @link     https://github.com/uestla/YetORM
 */

namespace YetORM;

use Nette;
use Nette\Database\Table\Selection as NSelection;
use Nette\Utils\Callback as NCallback;

class EntityCollection extends Nette\Object implements Collection
{

	/** @var NSelection|null */
	protected $selection;

	/** @var string|NCallback|null */
	protected $entity;

	/** @var string|null */
	protected $refTable;

	/** @var string|null */
	protected $refColumn;

	/** @var Entity[] */
	protected $data;

	/** @var int */
	private $count;

	/** @var int */
	private $countDistinct;

	/** @var array */
	private $keys;

	/**
	 * @param NSelection $selection
	 * @param string|callable|null $entity
	 * @param string $refTable
	 * @param string $refColumn
	 */
	public function __construct(NSelection $selection = NULL, $entity = NULL, $refTable = NULL, $refColumn = NULL)
	{
		$this->selection = $selection;
		$this->refTable = $refTable;
		$this->refColumn = $refColumn;

		try {
			NCallback::check($entity);
			$this->entity = NCallback::closure($entity);

		} catch (\Exception $e) {
			$this->entity = $entity;
		}
	}

	/** @return array */
	public function toArray()
	{
		return iterator_to_array($this);
	}

	/**
	 * @return $this
	 */
	public function resetOrder()
	{
		// it can't be directly set on the \Nette\Database\Table\Selection (v2.3@beta)
		$builder = $this->selection->getSqlBuilder();
		$builder->setOrder([], []);
		return $this;
	}

	/**
	 * API:
	 *
	 * <code>
	 * $this->orderBy('column', Collection::DESC); // ORDER BY [column] DESC
	 * // or
	 * $this->orderBy([
	 *    'first'  => Collection::ASC,
	 *    'second' => Collection::DESC,
	 * ]); // ORDER BY [first], [second] DESC
	 * </code>
	 *
	 * @param string|array $column
	 * @param bool $direction
	 * @return $this
	 */
	public function orderBy($column, $direction = NULL)
	{
		if (func_num_args() >= 3 && func_get_arg(2) === TRUE) {
			trigger_error('Third argument of ' . __METHOD__ . ' is deprecated. Use ::resetOrder instead.', E_USER_DEPRECATED);
			$resetOrder = TRUE;
		} else {
			$resetOrder = FALSE;
		}

		if ($this->selection) {
			if (is_array($column)) {
				foreach ($column as $col => $d) {
					$this->orderBy($col, $d, $resetOrder);
				}

			} else {
				// Back-compatibility
				if ($resetOrder) {
					$this->resetOrder();
				}

				$direction === NULL && ($direction = static::ASC);
				$sqlDirection = ($direction === static::DESC ? ' DESC' : '');

				if ($column instanceof FieldOrder) {
					$this->selection->order('FIELD(' . $column->getField() . ', ?)', $column->getValues());
				} else {
					$this->selection->order($column . $sqlDirection);
				}
			}

			$this->invalidate();
		}

		return $this;
	}

	/**
	 * @param int $limit
	 * @param int $offset
	 * @return $this
	 */
	public function limit($limit, $offset = NULL)
	{
		if ($this->selection) {
			$this->selection->limit($limit, $offset);
			$this->invalidate();
		}

		return $this;
	}

	/** @return void */
	public function rewind()
	{
		$this->loadData();
		$this->keys = array_keys($this->data);
		reset($this->keys);
	}

	/** @return Entity */
	public function current()
	{
		$key = current($this->keys);
		return $key === FALSE ? FALSE : $this->data[$key];
	}

	/** @return mixed */
	public function key()
	{
		return current($this->keys);
	}

	/** @return void */
	public function next()
	{
		next($this->keys);
	}

	/** @return bool */
	public function valid()
	{
		return current($this->keys) !== FALSE;
	}

	/** @return int */
	public function count()
	{
		if ($this->data !== NULL) {
			return count($this->data);
		}

		if (!$this->selection) {
			return 0;
		}

		if ($this->count === NULL) {
			$this->count = $this->selection->count('*');
		}

		return $this->count;
	}

	/**
	 * @param $column
	 * @return int
	 */
	public function countDistinct($column)
	{
		if (!$this->selection) {
			return 0;
		}

		if ($this->countDistinct === NULL) {
			$this->countDistinct = $this->selection->count("DISTINCT $column");
		}

		return $this->countDistinct;
	}

	/** @return void */
	private function loadData()
	{
		if ($this->data === NULL) {
			$this->data = [];

			if (!$this->selection) {
				return;
			}

			if ($this->entity instanceof \Closure) {
				$factory = $this->entity;

			} else {
				$class = $this->entity;
				$factory = function ($record) use ($class) {
					return new $class($record);
				};
			}

			foreach ($this->selection as $row) {
				$record = $this->refTable === NULL ? $row : $row->ref($this->refTable, $this->refColumn);
				$this->data[] = NCallback::invoke($factory, $record);
			}
		}
	}

	/** @return void */
	private function invalidate()
	{
		$this->data = NULL;
	}

}
